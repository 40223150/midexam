#!/usr/bin/env python
########################### 1. 導入所需模組
import cherrypy
import math
import os

########################### 2. 設定近端與遠端目錄
# 確定程式檔案所在目錄, 在 Windows 有最後的反斜線
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
# 設定在雲端與近端的資料儲存目錄
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    # 表示程式在雲端執行
    download_root_dir = os.environ['OPENSHIFT_DATA_DIR']
    data_dir = os.environ['OPENSHIFT_DATA_DIR']
else:
    # 表示程式在近端執行
    download_root_dir = _curdir + "/local_data/"
    data_dir = _curdir + "/local_data/"

########################### 3. 建立主物件
pi=math.pi

class coordinate(object):
    @cherrypy.expose
    def index(self):
        return self.Dposition()

    @cherrypy.expose
    def Dposition(self):
        outscene = """
        <form method="post" action="calculate">
        C點x座標:<input type=text name=Cx value=0><br />
        C點y座標:<input type=text name=Cy value=0><br />
        D點x座標:<input type=text name=Dx value=0><br />
        D點y座標:<input type=text name=Dy value=0><br />
        指定旋轉角度:<input type=text name=A value=0><br />
        <input type=submit value="完成"><input type=reset value="重置">
        </form>
        """
        return outscene
    @cherrypy.expose
    def calculate(self,Cx=None,Cy=None,Dx=None,Dy=None,A=None):
        r=math.sqrt(( int(Dx) - int(Cx) )**2 +( int(Dy) - int(Cy) )**2 )
        #print(r)
        angle=float(int(A)*pi/180)
        NDx=int(Cx)+r*math.cos(angle)
        NDy=int(Cy)+r*math.sin(angle)
        #ND=(NDx,NDy)
        return "當逆時針旋轉"+A+"度時,D點座標為("+str(NDx)+","+str(NDy)+")"

########################### 4. 安排啟動設定
# 配合程式檔案所在目錄設定靜態目錄或靜態檔案
application_conf = {'/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"},
        '/downloads':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': data_dir+"/downloads"}
    }

########################### 5. 在近端或遠端啟動程式
# 利用 HelloWorld() class 產生案例物件
root = coordinate()
# 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    # 雲端執行啟動
    application = cherrypy.Application(root, config = application_conf)
else:
    # 近端執行啟動
    '''
    cherrypy.server.socket_port = 8083
    cherrypy.server.socket_host = '127.0.0.1'
    '''
    cherrypy.quickstart(root, config = application_conf)
